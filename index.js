console.log("Hello World")

let firstName = "John";
let lastName = "Smith";
let age = 30;

console.log("FirstName: " + firstName);
console.log("LastName: " + lastName);
console.log("Age: " + age);

/*let hobbies = "Hobbies:"
let details = ["Biking", "Mountain Climbing", "Swimming"];
console.log(Hobbies)
console.log(details)*/

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log("Hobbies:");
console.log(hobbies);

/*let WorkAddress = "Work Address:";
let info = {
		houseNumber: '32',
		street: 'Washington',
		city: "Lincoln",
		state: "Nebraska"
}
console.log(WorkAddress);
console.log(info);*/

let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska",
}
console.log("Work Address:");
console.log(workAddress);
// Question about the output: Followed same codebase but the output still has single quoute for Array and object compared to double quote in expected output.

/*function printUserInfo(firstName, lastName, age,){
	console.log(firstName + ' ' + lastName + ' ' + "is " + age + " years of age.");
};
printUserInfo("John", "Smith", "30");*/


function printUserInfo(firstName, lastName, age,){
	console.log(firstName + ' ' + lastName + ' ' + "is " + age + " years of age.");
	console.log(hobbies);
	console.log(workAddress);
};
printUserInfo("John", "Smith", "30");


/*function returnFunction(isMarried){
	return isMarried
	console.log("This message will not be printed")	
}


console.log(returnFunction(true));

let isMarried = returnFunction("The value of isMarried is: true");
console.log(isMarried);*/

function returnFunction() {
	return true;
}

let isMarried = returnFunction()
console.log("The value isMarried: " + isMarried);